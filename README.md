# func-core

*func-core* is a Rust procedural macro inspired by Gary Bernhardt's screencast on [Functional core, imperative shell](https://www.destroyallsoftware.com/screencasts/catalog/functional-core-imperative-shell).

This macro will act on named structs and create a method new(), that takes all struct members as parameters and return a new object/instance of that struct, and a getter method for each struct member, that takes a self reference (&self) and returns the value of that member.

## Example

```rust
#[derive(FunctionalCore)]
struct Test {
    foo: u8,
    bar: f32
}

fn main() {
    let my_test = Test::new(1,2_f32);
    // each member has its specific getter that you can use
    assert_eq!(my_test.foo(), &1);
    assert_eq!(my_test.bar(), &2_f32);
}
```

## Usage

Add `func_core = "*"` as a dependency to your Cargo.toml and run `cargo build` to download it. After that, put a `use func_core::FunctionalCore` atop your Rust files to be able to use the derive macro.

## Why?

Laziness.
