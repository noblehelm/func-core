use func_core::FunctionalCore;

#[derive(FunctionalCore)]
struct Generic<T> {
    foo: T,
    bar: Vec<T>,
}

#[test]
fn test_t() {
    let tt: Generic<u8> = Generic::new(2_u8, vec![1, 2, 3]);
    assert_eq!(tt.foo(), &2_u8);
}

#[test]
fn test_vec_t() {
    let tvec: Generic<f32> = Generic::new(1_f32, vec![1_f32, 2_f32, 3_f32]);
    assert_eq!(tvec.bar(), &[1_f32, 2_f32, 3_f32]);
}
