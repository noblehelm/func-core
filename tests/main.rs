extern crate func_core;

use func_core::FunctionalCore;

#[derive(FunctionalCore)]
struct Test {
    foo: i8,
    bar: String,
    baz: f32,
    vektor: Vec<u8>,
}

#[test]
fn main_test() {
    let muh_test = Test::new(8, "hurr_durr".to_string(), 89.32, vec![1, 2, 3]);
    assert_eq!(muh_test.foo(), &8);
    assert_eq!(muh_test.bar(), &"hurr_durr");
    assert_eq!(muh_test.baz(), &89.32);
    assert_eq!(muh_test.vektor(), &vec![1, 2, 3]);
}
