use proc_macro2;
use quote::quote;
use syn;
use syn::{Fields, Ident, Type, TypeGenerics};

pub fn new(name: &Ident, fields: &Fields, generics: &TypeGenerics) -> proc_macro2::TokenStream {
    if let syn::Fields::Named(syn::FieldsNamed { ref named, .. }) = fields {
        let field_names: Vec<Option<Ident>> = named.clone().into_iter().map(|x| x.ident).collect();
        quote! {
            pub fn new(#(#named),*) -> #name #generics {
                #name {
                    #(#field_names),*
                }
            }
        }
    } else {
        panic!("This macro only works for named structs!")
    }
}

pub fn getters(fields: &Fields) -> proc_macro2::TokenStream {
    if let syn::Fields::Named(syn::FieldsNamed { ref named, .. }) = fields {
        let fields_names: Vec<Option<Ident>> = named.clone().into_iter().map(|x| x.ident).collect();
        let function_return: Vec<Type> = named.clone().into_iter().map(|x| x.ty).collect();
        let function_body = fields_names.clone();
        quote! {
            #(
                pub fn #fields_names(&self) -> &#function_return {
                    &self.#function_body
                }
            )*
        }
    } else {
        panic!("This macro only works for named structs!")
    }
}
