extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;

mod generate;

#[proc_macro_derive(FunctionalCore)]
pub fn derive_functional_core(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as syn::DeriveInput);
    let (impl_generics, ty_generics, where_clause) = &input.generics.split_for_impl();
    let struct_name: &syn::Ident = &input.ident;
    if let syn::Data::Struct(syn::DataStruct { ref fields, .. }) = input.data {
        let new = generate::new(struct_name, fields, ty_generics);
        let getters = generate::getters(fields);
        let expanded = quote! {
            impl #impl_generics #struct_name #ty_generics #where_clause {
                #new
                #getters
            }
        };
        // panic!(TokenStream::from(expanded).to_string())
        expanded.into()
    } else {
        panic!("Functional core macro is implemented only for named structs!");
    }
}
